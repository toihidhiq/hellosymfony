<?php

namespace App\Controller;


use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use Symfony\Flex\Response ;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;


use App\Service\MessageGenerator;
class ProductController extends AbstractController
{
    public function index()
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }

    /**
     * @Route("/product", name="productList")
     * @IsGranted("ROLE_ADMIN")
     */
    public function list(ProductRepository $productRepository)
    {
        $products = $productRepository->findAll();
        if (!$products) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/product/new", name="createProduct")
     * @IsGranted("ROLE_ADMIN")
     */

    public function createProduct(EntityManagerInterface $entityManager, MessageGenerator $messageGenerator)
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
        //$entityManager = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setName('Keyboard');
        $product->setPrice(1999);
        $product->setDescription('Ergonomic and stylish!');

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);
        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        $insertId = $product->getId();
        if ( $insertId ){
            $message = $messageGenerator->getHappyMessage( $insertId );
            $this->addFlash('success', $message );
        }

        //return new Response('Saved new product with id '.$product->getId());
        return $this->redirectToRoute('productList');
    }


    /**
     * @Route("/product/view/{id}", name="viewProduct")
     * @IsGranted("ROLE_ADMIN")
     * @param $id
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function view($id, ProductRepository $productRepository)
    {
        $product = $productRepository
            ->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        return $this->render('product/show.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/product/show/{id}", name="product_show", methods={"GET"})
     */
    public function show(Product $product)
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }


}
