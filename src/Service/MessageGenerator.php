<?php
// src/Service/MessageGenerator.php
namespace App\Service;
use Psr\Log\LoggerInterface;

class MessageGenerator
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getHappyMessage($id)
    {
        $ms = "Product $id is saved successfully";
        $this->logger->info($ms);
        return $ms;
    }
}