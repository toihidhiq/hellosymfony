<?php

namespace App\Service;

use App\Entity\Product;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Service\MessageGenerator;

class DataProvider extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function list($model)
    {
        $model = "$model::class";
        $items = $this->getDoctrine($model)
            ->getRepository($model)
            ->findAll();

        return $this->render('product/index.html.twig', [
            'items' => $items,
        ]);

    }

}